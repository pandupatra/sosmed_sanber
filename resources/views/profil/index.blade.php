@extends('master')

@section('content')
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-lg-4">
              <div class="text-center pb-4">
                <img src="" alt="" class="img-lg rounded-circle mb-3">
                <div class="mb-3">
                  {{ auth()->user()->username }}
                </div>
                <p class="w-75 mx-auto mb-3 border-bottom">
                  This is profile bio placeholder
                </p>
                <div class="d-flex justify-content-center">
                  <a href="/profil/edit" class="btn btn-primary">Edit Profile</a>
                </div>
              </div>
            </div>
            <div class="col-lg-8">
              <div class="profile-feed">
                <div class="d-flex align-items-start profile-fee-item">
                  <img src="" alt="" class="img-sm rounded-circle">
                  <div class="ml-4">
                    <h6>{{ auth()->user()->username }}</h6>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Similique iure facere ab dolore quod repellendus deserunt corporis quia impedit totam rerum mollitia rem, eum at est. Earum commodi unde itaque.</p>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection