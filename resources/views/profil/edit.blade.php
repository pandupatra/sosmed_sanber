@extends('master')
@section('judul')
    Edit Profil
@endsection
@section('content')

<form action="/profil/edit" method="put">
    
    @csrf
    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" name="nama" id="nama" value="{{ old('nama') }}" placeholder="Masukkan Nama Cast">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Jenis Kelamin </label>
        <select name="gender" class="form-control">
            <option value="">-pilih-</option>
            <option value="Laki-laki">Laki-laki</option>
            <option value="Perempuan">Perempuan</option>
            </select>
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Umur</label>
        <input type="number" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur Cast">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Bio</label>
        <input type="text" class="form-control" name="bio" id="bio" placeholder="Masukkan Bio">
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Tanggal Lahir</label>
        <input type="text" class="form-control" name="ultah" id="ultah"  placeholder="Masukkan Tanggal Lahir">
        @error('ultah')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Ubah</button>
</form>


@endsection