<form action="/post" method="post">
  @csrf
  <div>
    <textarea name="post" class="form-control" id="post" cols="200" rows="5" placeholder="Post something here..."></textarea>
    <button class="btn btn-primary my-2" type="submit">Post</button>
  </div>
</form>