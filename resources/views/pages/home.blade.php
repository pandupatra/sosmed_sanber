@extends('master')

@section('content')
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="d-flex align-items-start">
            <img src="" alt="" class="img-sm rounded-circle">
            <div class="ml-4">
              @include('post.create')
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row mt-4">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="mt-2 mb-5 profile-feed-header">
            <h2>{{ auth()->user()->username }}'s feeds</h2>
          </div>
          @foreach ($posts as $post)
              @if ($post->user_id == auth()->user()->id)
                <div class="border-bottom mb-5 pb-5">
                  <div class="d-flex align-items-start">
                    <img src="" alt="" class="img-sm rounded-circle">
                    <div class="ml-4">
                      {{$post->post}}
                    </div>
                  </div>
                </div>
              @endif
          @endforeach
        </div>
      </div>
    </div>
  </div>
@endsection