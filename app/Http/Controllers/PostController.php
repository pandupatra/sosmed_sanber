<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function create() {
        return view('post.show');
    }

    public function store(Request $request) {
        $request->validate([
            'post' => 'required|max:500',
        ]);

        Post::create([
            'post' => $request['post'],
            'user_id' => Auth::id()
        ]);
        return redirect('/home');
    }
}
