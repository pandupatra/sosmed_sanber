<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function index() {
        return view('register.index', [
            'title' => 'Register',
            'active' => 'register',
        ]);
    }

    public function store(Request $request) {
        $validatedData = $request->validate([
            'username' => 'required|unique:users|max:100',
            'email' => 'required|email:dns|unique:users',
            'password' => 'required|min:5|:max:100',
        ]);

        $validatedData['password'] = Hash::make($validatedData['password']);
        User::create($validatedData);

        return redirect('/login')->with('success', 'Registration success! You can login now');
    }
}
