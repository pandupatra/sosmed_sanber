<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    public function user() {
        return $this->hasOne('App\User');
    }
    protected $table = 'profil';
    protected $primaryKey = 'id';

    protected $fillable = [
        'nama',
        'umur',
        'gender',
        'bio',
        'status',
        'ultah'
    ];
}
