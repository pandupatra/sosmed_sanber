<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function user() {
        return $this->hasMany('App\User');
    }

    protected $table = 'post';
    
    protected $fillable = [
        'post',
        'user_id'
    ];
}
