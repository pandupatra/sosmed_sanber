<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'LoginController@index');

// Home Route
Route::get('/home', 'HomeController@index')->middleware('auth');

// Register Route
Route::get('/register', 'RegisterController@index')->middleware('guest');
Route::post('/register', 'RegisterController@store');

// Login Route
Route::get('/login', 'LoginController@index')->name('login')->middleware('guest');
Route::post('/login', 'LoginController@authenticate');
Route::post('/logout', 'LoginController@logout');

//Profile Route
Route::get('/profil', 'ProfilController@index');
Route::get('/profil/{profil}/edit', 'ProfilController@edit')->name('edit');
Route::get('/profil/edit', 'ProfilController@edit')->name('edit');
